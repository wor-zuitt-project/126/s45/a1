/*
	This file, first import Card and Button from react-bootstrap
	Create a Course component with the following features: 
	One Card component
	Inside the Card component, Create a Card.Body component,
	Inside the Card.Body component, create a Card.Title component with
	the name of a sample course (user any course name)
	Inside the Card.Body component, create a Card.Text component
	Inside the Card.Text component, create an h3 component with the word "Description"
	Inside the Card.Text component, create a p component with ANY placeholder text below the h3 component
	Inside the Card.Text component, create an h4 component with the word "Price" below the p component
	Inside the Card.Text component, create a p component with any price 
	
	Inside the Card.Text component, create a Button component with a variant attribute 
	with a value of "primary" that says "Enroll"
	Import the created Course Component in App.js. Comment out the Home component and add the Course component in the return statement
	Make sure to check your output

	40 Mins including submit
*/


import {Row, Col, Card, Button} from 'react-bootstrap';

export default function Course(){
	return (
		<>
			<Row>
				<Col xs={12} md={4}>
					<Card>
						<Card.Body>
							<Card.Title>
								<h2>React For Beginner</h2>
							</Card.Title>
							<Card.Text>
								<h3>Description</h3>
								<p> Text Holder Example </p>
								<h4>Price</h4>
								<p>$100</p>
								<Button className="Primary">Enroll</Button>
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</>
		)}